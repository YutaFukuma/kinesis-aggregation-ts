import { Kinesis } from 'aws-sdk'

import {
	aggregate as origaggregate,
	deaggregateSync as origdeaggregate,
	UserRecord,
	EncodedRecord,
} from 'aws-kinesis-agg'

export interface RecordPayload {
	data: string
	partitionKey: string
	explicitPartitionKey?: string
	sequenceNumber: string
}

export { UserRecord, EncodedRecord }

export const aggregate = (
	records: unknown[],
	encodedRecordHandler?: (
		encodedRecord: EncodedRecord,
	) => Promise<Kinesis.PutRecordOutput>,
	queueSize = 1,
) =>
	new Promise<EncodedRecord[]>((resolve, reject) => {
		const encodedRecords: EncodedRecord[] = []
		const handler = (
			encodedRecord: EncodedRecord,
			callback: (err?: Error, data?: Kinesis.PutRecordOutput) => void,
		) => {
			encodedRecords.push(encodedRecord)
			if (!encodedRecordHandler) return callback()
			encodedRecordHandler(encodedRecord).then(
				r => callback(undefined, r),
				x => callback(x),
			)
		}
		const done = () => resolve(encodedRecords)
		origaggregate(records, handler, done, reject, queueSize)
	})

export const deaggregate = (
	kinesisRecord: RecordPayload,
	computeChecksums: boolean,
) =>
	new Promise<UserRecord[] | undefined>((resolve, reject) =>
		origdeaggregate(
			kinesisRecord as ReturnType<typeof JSON.parse>,
			computeChecksums,
			(e, d) => {
				if (e) return reject(e)
				resolve(
					d?.map(r => {
						if ('string' === typeof r.data)
							r.data = Buffer.from(r.data, 'base64')
						return r
					}),
				)
			},
		),
	)
